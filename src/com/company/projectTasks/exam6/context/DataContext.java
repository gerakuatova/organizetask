package com.company.projectTasks.exam6.context;
import com.company.projectTasks.exam6.domain.Task;
import com.company.projectTasks.exam6.domain.TasksDataModel;
import java.util.Optional;

public interface DataContext {

    Optional<Task> getTask() ;
    boolean add(TasksDataModel task);
}
